const omdbApiMovie = require("./omdbapi")
const logger = require("../logs/config")
const Movie = require("../models/movie");

const movieSearch = async(ctx) => {

    const { search } = ctx.request.params
    const { year } = ctx.header

    let movieAPI = {}

    await omdbApiMovie(search,year)
        .then(resp => {movieAPI = resp})
        .catch((err) => logger.error(`Error al traer los datos de omdbapi: ${err}`));

    const { Response,Error,Title,Year,Released,Genre,Director,Actors,Plot,Ratings } = movieAPI
    if (Response == 'False'){   // lo toma como string   
        logger.log({"level": "error", "message": `*${ new Date().toJSON() } *movie.js *movieSearch *request: search: ${ search } year: ${ year }  *response: ${ Error }`})
        ctx.status = 400
        ctx.body = {
            Error
        }
    } else {
        const existeMovie = await Movie.findOne({Title})  // busca si existe en la BD
        
        if ( existeMovie ){
            const {Title,Year,Released,Genre,Director,Actors,Plot,Ratings } = existeMovie
            ctx.body = {
                Title,Year,Released,Genre,Director,Actors,Plot,Ratings
            }
        } else {
            const movie = new Movie({Title,Year,Released,Genre,Director,Actors,Plot,Ratings})
            await movie.save();
            ctx.body = {
                Title,Year,Released,Genre,Director,Actors,Plot,Ratings
            }
        }
    }
}

const movieList = async(ctx) =>{

    const { page } = ctx.header
    //console.log ('page',page)
    const total = await Movie.countDocuments()
    const limit = 5  // para paginar de 5 en 5
    const skip = (page-1)*limit
    let total_page
    let movies = {}

    if (total > limit){
        total_page = parseInt((total/limit)+1);
        movies = await Movie.find()
                                .skip(skip)
                                .limit(limit)
    } else {
        total_page = 1
        movies = await Movie.find()     
    }

    if (page > total_page){
        let Error = `El total de páginas es hasta ${total_page}`
        logger.log({"level": "error", "message": `*${ new Date().toJSON() } *movie.js *movieList *request: page: ${ page } *response: ${ Error }`})
        ctx.status = 400
        ctx.body = {
            Error
        }
    } else {
        ctx.body = {
            total,
            total_page,
            movies
        }
    } 
}

const searchReplace = async(ctx) => {

    const { movie,find,replace } = ctx.request.body

    const existeMovie = await Movie.findOne({ Title: movie })
    if ( existeMovie ){
        const { Title, Plot } = existeMovie
        try {
            let newPlot = Plot.replaceAll(find,replace)
            await Movie.findOneAndUpdate(Title, {Plot : newPlot})
            ctx.body = {
                movie,
                find,
                replace,
                Plot,
                newPlot
            }
        } catch (error) {
            let Error = 'No se Pudo Remplazar'
            logger.log({"level": "error", "message": `*${ new Date().toJSON() } *movie.js *searchReplace *request: movie: ${ movie } find: ${ find } replace: ${ replace } *response: ${ error }`})
            ctx.status = 400
            ctx.body = {
                Error
            }
        }
    } else {
        let Error = 'No se encuentra la Pelicula'
        logger.log({"level": "error", "message": `*${ new Date().toJSON() } *movie.js *searchReplace *request: movie: ${ movie } find: ${ find } replace: ${ replace } *response: ${ Error }`})
        ctx.status = 400
        ctx.body = {
            Error
        }
    }

}

module.exports = {
    movieSearch,
    movieList,
    searchReplace
}