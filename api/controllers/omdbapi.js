const axios = require("axios");
const logger = require("../logs/config")

const urlApi = `https://www.omdbapi.com/?`

const omdbApiMovie = async(search,year) => {
    let bySearch = `t=${search}`;
    let byYear = `y=${year}`
    let apikey = `apikey=${process.env.API_KEY_OMDBAPI}`
    let respuesta = {}
    let url = `${urlApi}${bySearch}&${byYear}&${apikey}`;
    
    //console.log('url',url)
    await axios.get(url)
        .then(response => {
            //console.log(response.data);
            respuesta = response.data
    })
    .catch(err => {
        logger.error(`Error al traer los datos de omdbapi: ${err}`)
    })

    return respuesta;
}

module.exports = omdbApiMovie