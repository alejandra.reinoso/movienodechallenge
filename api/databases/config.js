const mongoose = require('mongoose')
const logger = require('../logs/config');

const dbconnection = async() => {
    try{
        await mongoose.connect(process.env.MONGODB_CNN,{
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        })

        logger.info(`Conectado a la BD`)
    } catch (error) {
        logger.error(`Error a la hora de iniciar base de datos`)
        throw new Error(`Error a la hora de iniciar base de datos`)
    }
}

module.exports = dbconnection