const Joi = require('joi')

const schemas = {
    searchMovieParams: Joi.object({
        search: Joi.string().required()
    }),
    searchMovieHeader: Joi.object({
        year: Joi.number().min(4)
    }),
    movieListHeader: Joi.object({
        page: Joi.number().required()
    }),
    searchReplace: Joi.object({
        movie: Joi.string().required(),
        find: Joi.string().required(),
        replace: Joi.string().required()
    }),
    
};

module.exports = schemas;