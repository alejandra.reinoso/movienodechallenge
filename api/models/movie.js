const {Schema, model} = require('mongoose');

const RatingSchema = Schema({
    Source: {
        type: String,
        required: [true,'Source es obligatorio']
    },
    Value: {
        type: String,
    }
})

const MovieSchema = Schema({
    Title: {
        type: String,
        required: [true,'Title es obligatorio'],
        unique: true
    },
    Year: {
        type: String,
    },
    Released: {
        type: String,
    },
    Genre: {
        type: String,
    },
    Director: {
        type: String,
    },
    Actors: {
        type: String,
    },
    Plot: {
        type: String,
    },
    Ratings: {
        type: [RatingSchema],
    }
})


module.exports = model('Movie',MovieSchema)