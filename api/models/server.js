const Koa = require('koa')
const dbconnection = require('../databases/config')
const logger = require('../logs/config')
const json = require('koa-json')
const cors = require('@koa/cors')
const bodyParser = require('koa-bodyparser')
const routerMovie = require('../routers/movie')
const morgan = require('koa-morgan')
const swagger = require("swagger2")
const { ui, validate } = require("swagger2-koa")

class Server {
  constructor(){
    this.app = new Koa();
    this.port = process.env.PORT;
    this.swaggerDocument = swagger.loadDocumentSync("api/swagger/api.yaml");

    // BD
    this.connectionDB()

    //MIDDLEWARES
    this.middlewares()
    
    //ROUTERS
    this.routes();
  }

  async connectionDB(){
    await dbconnection()
  }

  routes(){
    this.app.use(routerMovie.routes()).use(routerMovie.allowedMethods())

    //swagger
    this.app.use(ui(this.swaggerDocument, "/swagger"))
    this.app.use(validate(this.swaggerDocument))
  }

  middlewares(){
    // CORS
    this.app.use(cors())
    // logger HTTP 
    this.app.use(morgan('tiny'))
    // Lectura y parseo del body
    this.app.use(json())
    this.app.use(bodyParser());
    // Directorio Publico
    //this.app.use(koastatic('public'))
}

  listen(){
    this.app.listen(this.port, ()=> {
      logger.info(`Servidor corriendo en puerto ${this.port}`)
    })
  }
}

module.exports = Server