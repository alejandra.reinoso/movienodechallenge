const Router = require('@koa/router');
const { movieSearch, movieList, searchReplace } = require('../controllers/movie');
const { validaCampos } = require('../middlewares/validator');
const schemas = require('../helpers/schemas');

const router = new Router();

router.get('/movie/:search',
    validaCampos(schemas.searchMovieHeader,'header'),
    validaCampos(schemas.searchMovieParams,'params')
,movieSearch)

router.post('/movie',
    validaCampos(schemas.searchReplace,'body'),
searchReplace)

router.get('/movie',
    validaCampos(schemas.movieListHeader,'header')
,movieList)

module.exports = router;